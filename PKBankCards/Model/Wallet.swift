
//
//  Wallet.swift
//  PKBankCards
//
//  Created by divya kothagattu on 03/09/20.
//  Copyright © 2020 divya kothagattu. All rights reserved.
//

import Foundation

class Wallet: ObservableObject {
    @Published var cards: [Card]
        
    init(cards: [Card]) {
        self.cards = cards.reversed()
    }
    
    func index(of card: Card) -> Int {
        return cards.count - cards.firstIndex(of: card)! - 1
    }
    
    func isFirst(card: Card) -> Bool {
        return index(of: card) == 0
    }
}
